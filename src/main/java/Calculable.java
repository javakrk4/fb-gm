/**
 * Created by Filip on 2017-07-19.
 */
public interface Calculable {

    int add(int a, int b);
    int substract(int a, int b);
    int multiply(int a, int b);
    int divide(int a, int b);
}
